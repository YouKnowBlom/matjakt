<html>

<head>
    <link rel="stylesheet" type="text/css" href="style.css">
    <title></title>
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
</head>

<body>
    <header>
        <!-- logga in -->
        <a href="index.php"><h1 id="logga">Matjakt</h1></a>
        <nav>
            <a href="index.php">Meny</a>
            <a href="login.php">Login</a>
        </nav>
    </header>
    <section id="bodyContainer">
        <section id="searchContainer">
            <form method="GET" action="sok.php" id="searchBar">
                <input name="q" type="text" id="searchInput" placeholder="Sök efter matvaror">
            </form>
        </section>
        <section id="offerContainer">
            <aside id="butiksinfo">
                <h1>
                Butiksnamn
            </h1>
                <p>
                    Adress: Östra Banvägen 64, 183 30 Täby
                </p>
                <iframe width="300" height="200" frameborder="0" style="border:0" src="https://www.google.com/maps/embed/v1/place?key=AIzaSyD2GWBkMwU4PwY76Peoo_FQ8Rox7Gn3AkY &q=Tempo+Roslags+Nasby, Stockholm+Sweden" allowfullscreen>
                </iframe>
            </aside>
            <section class="offerContainer-sections" id="productContainerStore">
                <h2>Erbjudanden</h2>
                <div class="productContainer">
                    <!-- php kod här -->
                    <img class="productImage64" src="bilder/tomat64.jpg">
                    <p class="productTitle">Tomat</p>
                    <p class="productPrice underlined">30kr</p>
                    <p class="productPrice">20kr</p>
                </div>
                <div class="productContainer">
                    <!-- php kod här -->
                    <img class="productImage64" src="bilder/kassler64.jpg">
                    <p class="productTitle">Kassler</p>
                    <p class="productPrice underlined">40kr</p>
                    <p class="productPrice" >20kr</p>
                </div>
                <div class="productContainer">
                    <!-- php kod här -->
                    <img class="productImage64" src="bilder/tunnbr%C3%B6d64.jpg">
                    <p class="productTitle">Tunnbröd</p>
                    <p class="productPrice underlined">30kr</p>
                    <p class="productPrice">26kr</p>
                </div>
                <div class="productContainer">
                    <!-- php kod här -->
                    <img class="productImage64" src="bilder/gurka64.jpg">
                    <p class="productTitle">Gurka</p>
                    <p class="productPrice underlined">10kr</p>
                    <p class="productPrice">8kr</p>
                </div>
            </section>
        </section>
        <footer>
            <p>&copy; Matjakt.com 2017</p>
        </footer>
    </section>
</body>

</html>