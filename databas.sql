-- --------------------------------------------------------
-- Värd:                         127.0.0.1
-- Server version:               5.6.21 - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL Version:             9.1.0.4867
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping database structure for matsmart
CREATE DATABASE IF NOT EXISTS `matsmart` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `matsmart`;


-- Dumping structure for table matsmart.mat
CREATE TABLE IF NOT EXISTS `mat` (
  `label` tinytext NOT NULL,
  `price` double NOT NULL,
  `type` tinytext NOT NULL,
  `store` tinytext,
  `similarWares` tinytext NOT NULL,
  `amount` tinytext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table matsmart.mat: ~6 rows (approximately)
/*!40000 ALTER TABLE `mat` DISABLE KEYS */;
INSERT INTO `mat` (`label`, `price`, `type`, `store`, `similarWares`, `amount`) VALUES
	('Kronfågeln', 30, 'kyckling', 'Ica Näsbydal', '["ris", "chilisås"]', '1kg'),
	('Heinz', 40, 'chilisås', 'Ica dragonen', '["kyckling", "ris"]', '0.3L'),
	('Uncle ben', 35, 'ris', 'willys', '["kyckling", "chilisås"]', '5kg'),
	('Garant', 400, 'kyckling', 'Willys', '[""]', '2kg'),
	('Eldorado', 10, 'kyckling', 'Willys', '', '10kg'),
	('X-TRA', 60, 'kyckling', 'Tempo', '', '1kg');
/*!40000 ALTER TABLE `mat` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
