<html>
<head>
	<link rel="stylesheet" type="text/css" href="style.css">
	<title></title>
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
</head>
<body>
	<header>
		<!-- logga in -->
        <a href="index.php"><h1 id="logga">Matjakt</h1></a>
        <nav>
            <a href="index.php">Meny</a>
            <a href="login.php">Login</a>
        </nav>
	</header>
	<section id="bodyContainer">
		<section id="searchContainer">
			<form method="GET" action="sok.php" id="searchBar">
				<input name="q" type="text" id="searchInput" placeholder="Sök efter matvaror">
			</form>
		</section>
		<section id="offerContainer">
            <section id="loginSection">
                <h1 id="loginHeader">
                    Logga in
                </h1>
                <form>
                    <label for="username">Användarnamn</label>
                    <input type="text" id="usernameInput" name="username">
                    <label for="password">Lösenord</label>
                    <input type="password" id="passwordInput" name="password">
                    <input type="submit">
                </form>
                <p>
                    Inget Konto?
                </p>
                <form>
                    <input type="button" value="Registrera dig">
                </form>
            </section>
		</section>
        <footer>
            <p>&copy; Matjakt.com 2017</p>
        </footer>
	</section>
</body>
</html>