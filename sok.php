<html>
<head>
    <link rel="stylesheet" type="text/css" href="style.css">
    <title></title>
    <meta charset="utf-8">
    <script type="text/javascript" src="sorttable.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
</head>
<body>
    <header>
        <!-- LOGGA -->
        <!-- logga in -->
        <a href="index.php"><h1 id="logga">Matjakt</h1></a>
        <nav>
            <a href="index.php">Meny</a>
            <a href="login.php">Login</a>
        </nav>
    </header>
    <section id="bodyContainer">
        <section id="searchContainer">
            <form method="GET" action="sok.php" id="searchBar">
                <input name="q" type="text" id="searchInput" placeholder="Sök efter matvaror">
            </form>
        </section>
        <section id="offerContainer">
            <div id="filterContainer">
                <label for="prisSlider">Pris:</label>
                <input type="range" name="prisSlider">
                <div id="filterBrandContainer">
                    <button>Täby</button>
                    <button>Åkersberga</button>
                    <button>Stockholm</button>
                    <button>Danderyd</button>
                </div>
            </div>
            
            <?php 
            if(isset($_GET["q"])){
                $query = $_GET["q"];
            } else {
                $query = "error";
            }
            $db = mysqli_connect("localhost", "root", "", "matsmart");
            if(!$db){
                die("Something went wrong! Error code: " . mysqli_connect_errno());
            }
            $getItems = 'SELECT * FROM mat WHERE type = "' . $query . '"';
            $results = $db->query($getItems);
            if($results->num_rows == 0){
                echo "Inget hittades";
            } else { 
                echo '
                <table class="sortable" id="results" cellspacing=0>
                    <tr>
                        <th>Märke</th>
                        <th>Pris</th>
                        <th>Mängd</th>
                        <th>pris/mängd</th>
                        <th>Affär</th>
                    </tr>';
                    if($results) {
                        while($row = $results->fetch_assoc()){
                            echo "<tr><td>" . $row["label"] . "</td><td>" . $row["price"] . " kr </td><td>" . $row["amount"] . "</td><td>" . ($row["price"] / $row["amount"]) . "<td>" . $row["store"] . "</td></tr>";
                        }
                    }
                    echo "</table>";
                }
                ?>
            </section>
        <footer>
            <p>&copy; Matjakt.com 2017</p>
        </footer>
        </section>
    </body>
    </html>